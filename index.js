require("dotenv").config();
const express = require("express");
const cors = require("cors");
const app = express();

const routes = require("./api/routes/index");
require("./api/db/db");

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(routes);

const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`Badger Server running!`));
