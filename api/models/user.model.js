"use strict";
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const User = new Schema({
    username: { type: String, required: true, trim: true },
    password: { type: String, required: true },
    firstname: { type: String, required: true, trim: true },
    lastname: { type: String, required: true, trim: true },
    email: { type: String, required: true, trim: true },
    birthday: { type: Date, required: false },
    lastLogin: { type: Date, required: false },
    trustPoints: { type: Number, required: true }
});

User.methods.generateAuthToken = function() {
    const token = jwt.sign(
        { _id: this._id, isAdmin: this.isAdmin },
        process.env.PVT_KEY
    );
    return token;
};

module.exports = mongoose.model("User", User);
