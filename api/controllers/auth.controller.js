const bcrypt = require("bcrypt");
const Service = require("../services/user.service");
const {
    nonExistingUser,
    wrongCredentials,
    existingUser,
    registerFail,
    invalidInformation
} = require("../responses/error/auth.error");
const { loggedIn, registered } = require("../responses/success/auth.success");
const { validate, userSchema } = require("../middleware/validations");
const User = require("../models/user.model");

login = async (req, res) => {
    const { username, password } = req.body;
    const user = await Service.findUser(username);
    if (!user) res.send(nonExistingUser);

    bcrypt.compare(password, user.password, (err, result) => {
        if (!result) res.send(wrongCredentials);
        else {
            const token = user.generateAuthToken();
            res.header("x-auth-token", token).send(
                loggedIn(user._id, user.username, user.email)
            );
        }
    });
};

register = async (req, res) => {
    const { error } = validate(req.body, userSchema);
    if (error)
        return res.status(invalidInformation.status).send(invalidInformation);

    const { username, password, email, firstname, lastname } = req.body;
    let user = await Service.findUser(username, email);
    if (user) return res.status(existingUser.status).send(existingUser);

    user = new User({ username, password, email, firstname, lastname });
    user.password = await bcrypt.hash(user.password, 10);
    user.lastLogin = new Date();
    user.trustPoints = 0;

    const reg = await Service.newUser(user);
    if(!reg) res.status(registerFail.status).send(registerFail);

    const token = user.generateAuthToken();
    res.header("x-auth-token", token).send(
        registered(user._id, user.username, user.email, user.trustPoints, user.lastLogin)
    );
};

module.exports = {
    login,
    register
};
