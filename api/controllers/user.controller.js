const bcrypt = require("bcrypt");
const Service = require("../services/user.service");
const User = require("../models/user.model");
const { validate, userSchema } = require("../middleware/validations");
const {
    invalidInformation,
    existingUser
} = require("../responses/error/user.error");
const {
    createdUser,
    returnUser
} = require("../responses/success/user.success");

getUser = (req, res) => {
    res.send("GET USER");
};

newUser = async (req, res) => {
    const { error } = validate(req.body, userSchema);
    if (error)
        return res.status(invalidInformation.status).send(invalidInformation);

    const { username, password, email, firstname, lastname } = req.body;
    let user = await Service.findUser(username);
    if (user) return res.status(existingUser.status).send(existingUser);

    user = new User({ username, password, email, firstname, lastname });
    user.password = await bcrypt.hash(user.password, 10);
    user.lastLogin = new Date();
    user.trustPoints = 0;

    await Service.newUser(user);

    const token = user.generateAuthToken();
    res.header("x-auth-token", token).send(
        createdUser(user._id, user.username, user.email, user.lastLogin, user.trustPoints)
    );
};

updateUser = (req, res) => {};

getAllUsers = (req, res) => {
    res.send("ALL USERS");
};

module.exports = {
    getUser,
    getAllUsers,
    newUser,
    updateUser
};
