const Joi = require("joi");

const userSchema = {
    username: Joi
        .string()
        .min(3)
        .max(50)
        .required(),
    email: Joi
        .string()
        .min(5)
        .max(255)
        .required()
        .email(),
    password: Joi
        .string()
        .min(3)
        .max(255)
        .required(),
    firstname: Joi
        .string()
        .min(3)
        .max(50)
        .required(),
    lastname: Joi
        .string()
        .min(3)
        .max(50)
        .required(),
    birthday: Joi
        .date()
        .greater("1-1-1950")
        .less("now"),
    trustPoints: Joi
        .number()
        .integer()
        .min(0),
    lastLogin: Joi
        .date()
        .greater("1-1-2019")
        .less("now"),

};

validate = (model, schema) => {
    return Joi.validate(model, schema);
};

module.exports = {
    validate,
    userSchema
};
