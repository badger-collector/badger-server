const Query = require("../db/user.queries");

getUser = id => {
    return "";
};

newUser = async user => {
    const res = await Query.newUser(user);
    return res;
};

updateUser = (id, newInfo) => {};

findUser = async (username, email) => {
    let emailRes = null;
    let userRes = null;
    if(email) emailRes = await Query.findByEmail(email);
    if(username) userRes = await Query.findByUsername(username);
    return (emailRes || userRes);
};

module.exports = {
    getUser,
    newUser,
    updateUser,
    findUser
};
