const Model = require("../models/user.model");

findById = async id => {
    const res = await Model.findById(id, (err, user) => {
        if (err) return err;
        return user;
    });
    return res;
};

findByUsername = async username => {
    const res = await Model.findOne({ username });
    return res;
};

findByEmail = async email => {
    const res = await Model.findOne({ email });
    return res;
}

newUser = async user => {
    const res = await Model.create(user);
    return res;
};

deleteUser = async id => {
    const res = await Model.deleteOne({ id });
    return res;
};

module.exports = {
    findById,
    findByUsername,
    newUser,
    deleteUser,
    findByEmail
};
