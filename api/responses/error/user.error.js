module.exports = {
    invalidInformation: {
        success: false,
        status: 400,
        message: "The submitted information is invalid",
        group: "users",
        tag: "invalidInformation"
    },
    existingUser: {
        success: false,
        status: 400,
        message: "This user already exists",
        group: "users",
        tag: "existingUser"
    },
    nonExistingUser: {
        success: false,
        status: 400,
        message: "This user does't exist",
        group: "users",
        tag: "nonExistingUser"
    }
};
