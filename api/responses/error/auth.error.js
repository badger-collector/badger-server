module.exports = {
    invalidInformation: {
        success: false,
        status: 400,
        message: "The submitted information is invalid",
        group: "auth",
        tag: "invalidInformation"
    },
    existingUser: {
        success: false,
        status: 400,
        message: "This user already exists",
        group: "auth",
        tag: "existingUser"
    },
    nonExistingUser: {
        success: false,
        status: 400,
        message: "This user does't exist",
        group: "auth",
        tag: "nonExistingUser"
    },
    wrongCredentials: {
        success: false,
        status: 401,
        message: "The credentials do not match",
        group: "auth",
        tag: "wrongCredentials"
    },
    registerFail: {
        success: false,
        status: 503,
        message: "Something is wrong with the server",
        group: "auth",
        tag: "registerFail"
    }
};
