createdUser = (_id, username, email) => {
    return {
        success: true,
        status: 201,
        group: "users",
        tag: "createdUser",
        data: {
            _id,
            username,
            email
        }
    };
};

returnUser = () => {
    return {
        success: true,
        status: 200,
        group: "users",
        tag: "returnUser",
        data: {
            _id,
            username,
            email
        }
    };
};

module.exports = {
    createdUser,
    returnUser
};
