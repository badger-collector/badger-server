loggedIn = (_id, username, email, firstname, lastname, birthday) => {
    return {
        success: true,
        status: 200,
        group: "auth",
        tag: "loggedIn",
        data: {
            _id,
            username,
            email,
            firstname,
            lastname,
            birthday
        }
    };
};

registered = (_id, username, email, firstname, lastname) => {
    return {
        success: true,
        status: 201,
        group: "auth",
        tag: "registered",
        data: {
            _id,
            username,
            email,
            firstname,
            lastname,
        }
    };
};

module.exports = {
    loggedIn,
    registered
};
