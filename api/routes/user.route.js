var router = require("express").Router();
const auth = require("../middleware/authentication");

const Controller = require("./../controllers/user.controller");

router
    .route("/")
    .get(auth, Controller.getAllUsers)
    .post(Controller.newUser);
router
    .route("/:id")
    .get(auth, Controller.getUser)
    .put(auth, Controller.updateUser);

module.exports = router;
