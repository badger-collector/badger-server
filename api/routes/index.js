var express = require("express");
var router = express.Router();

const users = require("./user.route");
const auth = require("./auth.route");

router.get("/", (req, res) => res.send("Hey!"));
router.use("/user", users);
router.use("/auth", auth);

module.exports = router;
